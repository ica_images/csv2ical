# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 16:41:12 2024

@author: passieux
"""

import numpy as np
import matplotlib.pyplot as plt
from icalendar import Calendar, Event, vCalAddress, vText
from datetime import datetime
import pytz
import os
import csv

 
# init the calendar
cal = Calendar()
# Some properties are required to be compliant
cal.add('prodid', '-//My calendar product//example.com//')
cal.add('version', '2.0')

# %%

csvfile = open('edt.csv', newline='')
spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
rows = [row for row in spamreader]

for row in rows:
    title = row[1]
    location = row[6]
    date = row[3]
    day, month, year = np.array(date.split('/')).astype('int')
    start = row[4]
    hour_s, min_s, sec_s = np.array(start.split(':')).astype('int')
    end = row[5]
    hour_e, min_e, sec_e = np.array(end.split(':')).astype('int')
    event = Event()
    event.add('summary', vText(title))
    event.add('dtstart', datetime(year, month, day, hour_s, min_s, sec_s, tzinfo=pytz.timezone('Europe/Paris')))
    event.add('dtend', datetime(year, month, day, hour_e, min_e, sec_e, tzinfo=pytz.timezone('Europe/Paris')))
    event['location'] = vText(location)
    event.add('priority', 5)
    cal.add_component(event)

# %% Write to disk
f = open('insa.ics', 'wb')
f.write(cal.to_ical())
f.close()
